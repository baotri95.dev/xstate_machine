import { Employee } from '_employee.machine/model/employee.interface'

const data = [
  { id: '1', name: 'Nguyễn Thị Hà', phone: '0939322111', old: '26' },
  { id: '2', name: 'Đỗ Bảo Trị', phone: '09827878344', old: '26' },
]

export const FetchEmployee = () => {
  return new Promise((resolve, rejects) => {
    setTimeout(() => {
      resolve(data)
    }, 1000)
  })
}

export const createEmployee = (employee: Employee) => {
  const id = Math.floor(Math.random() * 500).toString()
  const createAsyn = (resolve, rejects) => {
    if (employee.name.length) {
      if (id == employee?.id) {
        rejects({ code: '400', message: 'Lỗi trùng id' })
        return
      }
      setTimeout(() => {
        data.push({ ...employee, id })
        resolve({ ...data, message: 'Thêm thành công!' })
      }, 500)
      return
    }
    rejects({ code: '400', message: 'Thêm thất bại, xem lại giá trị nhập' })
  }
  return new Promise(createAsyn)
}

export const updateEmployee = employee => {
  const { id, ...rest } = employee
  const createAsyn = (resolve, rejects) => {
    setTimeout(() => {
      if (Object.values(employee).includes('')) {
        rejects({ code: '400', message: 'Thêm thất bại, xem lại giá trị nhập' })
        return
      }
      const index = data.findIndex(s => s.id == id)
      if (index != -1) {
        const data_ = data
        data_[index] = { ...rest, id }
        resolve({ ...data, message: 'Cập nhật thành công' })
        return
      }
      rejects({ code: 400, message: 'Lỗi! Cập nhật thất bại' })
    }, 500)
  }
  return new Promise(createAsyn)
}

export const deleteEmpolyee = id => {
  const createAsyn = (resolve, rejects) => {
    console.log(id)
    setTimeout(() => {
      const index = data.findIndex(s => s.id == id)
      if (index != -1) {
        data.splice(index, 1)
        resolve({ ...data, message: 'Xóa thành công!' })
        return
      }
      rejects({ code: 400, message: 'Lỗi hệ thống' })
    }, 500)
  }
  return new Promise(createAsyn)
}
