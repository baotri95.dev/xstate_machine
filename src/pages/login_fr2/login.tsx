import { useSelector } from '@xstate/react'
import { useEffect, useRef } from 'react'
import { Form } from 'react-bootstrap'
import { useHistory } from 'react-router'
import { login_fr2Service, login_Machine } from '_system.machine/machine'
import { mergeMeta } from '_system.machine/options'

const Login = (): JSX.Element => {
  const userInputRef = useRef(null)
  const passInputRef = useRef(null)

  const handleUserInputFocus = () => {
    userInputRef.current.focus()
  }
  const handlePassInputFocus = () => {
    passInputRef.current.focus()
  }
  const focusInput = () => {
    if (state.hasTag('userErr')) {
      handleUserInputFocus()
    } else if (state.hasTag('passwordErr')) {
      handlePassInputFocus()
    }
  }

  const { send } = login_fr2Service
  const state = useSelector(login_fr2Service, state => state)
  const history = useHistory()

  useEffect(() => {
    focusInput()
  }, [state])

  return (
    <div className="login-form">
      <Form>
        <h2 className="text-center">Log in (Xstate)</h2>
        <div className="form-group">
          <input
            type="text"
            className="form-control"
            ref={userInputRef}
            value={state.context.username}
            onChange={e => {
              send({
                type: 'cacheUserName',
                value: e.target.value,
              })
            }}
            placeholder="Username"
            autoFocus
          />
          <label style={{ color: 'red' }}>
            {state.hasTag('userErr') && (mergeMeta(state.meta) as any).message}
          </label>
        </div>
        <div className="form-group">
          <input
            type="password"
            className="form-control"
            ref={passInputRef}
            placeholder="Password"
            value={state.context.password}
            onChange={e => {
              send({
                type: 'cachePassword',
                value: e.target.value,
              })
            }}
          />
          <label style={{ color: 'red' }}>
            {state.hasTag('passwordErr') &&
              (mergeMeta(state.meta) as any).message}
          </label>
        </div>
        <div className="form-group">
          <button
            type="submit"
            className="btn btn-primary btn-block"
            onClick={e => {
              e.preventDefault()
              send({ type: 'SUBMIT' })
              // history.push('/admin/home')
            }}>
            Log in
          </button>
        </div>
        <div className="clearfix">
          <label className="pull-left checkbox-inline">
            user-name: baotri95.dev / password: 12345678
          </label>
        </div>
      </Form>
    </div>
  )
}
export default Login
