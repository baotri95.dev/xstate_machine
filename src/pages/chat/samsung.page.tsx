import { useSelector } from '@xstate/react'
import { IphoneService } from '_chat_machine/chat.machine'
import { useEffect, useState } from 'react'

const SAMSUNGPAGE = (): JSX.Element => {
  const inphoneState = useSelector(IphoneService, state => state)
  const wifiState = useSelector(
    inphoneState.context.iphoneMachine,
    state => state
  )
  const [samsung, setMachine] = useState(null)
  const [serviceSamsung, setSend] = useState(null)
  const conterChatClass = item => (item?.role == 1 ? 'darker' : '')

  useEffect(() => {
    if ((wifiState as any)?.children) {
      const stateSamsung = (wifiState as any)?.children?.client

      if (stateSamsung && Object.keys(stateSamsung).length) {
        setSend(stateSamsung)
        setTimeout(() => {
          ;(stateSamsung as any).subscribe(item => setMachine(item))
        })
      }
      //   const test = useSelector((wifiState as any)?.children, state => state)
      //   console.log('111', test)
    }
  })
  return (
    <div className="container-v">
      <div className="box-parent">
        <div className="card-body">
          <div className="card-samsung">
            <div>Samsung: {samsung?.context?.userName}</div>

            <div className="chat-view">
              {samsung?.context?.LIST_MESSAGE?.map((item, i) => (
                <div
                  className={'container-chat ' + conterChatClass(item)}
                  key={i}>
                  <p>{item?.message}</p>
                  <span className={item.role == 1 ? 'time-right' : 'time-left'}>
                    {item?.userName}
                  </span>
                </div>
              ))}
            </div>
            <div className="chat-box">
              <div className="form-chat">
                <input
                  value={samsung?.context?.message}
                  onChange={e =>
                    serviceSamsung.send({
                      type: 'CACHE_INPUT',
                      value: e.target.value,
                    })
                  }
                />
                <button
                  className="btn btn-primary"
                  onClick={() => serviceSamsung.send('POST')}>
                  Gửi
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SAMSUNGPAGE
