import { useSelector } from '@xstate/react'
import { IphoneService } from '_chat_machine/chat.machine'
import { useEffect, useState } from 'react'

const CHAT = (): JSX.Element => {
  const inphoneState = useSelector(IphoneService, state => state)
  const wifiState = useSelector(
    inphoneState.context.iphoneMachine,
    state => state
  )
  const [samsung, setMachine] = useState(null)
  const [serviceSamsung, setSend] = useState(null)
  const message_iphone = useSelector(
    IphoneService,
    state => state.context.LIST_MESSAGE
  )
  const conterChatClass = item => (item?.role == 1 ? 'darker' : '')
  useEffect(() => {
    if ((wifiState as any)?.children) {
      const stateSamsung = (wifiState as any)?.children?.client

      if (stateSamsung && Object.keys(stateSamsung).length) {
        setSend(stateSamsung)
        setTimeout(() => {
          ;(stateSamsung as any).subscribe(item => setMachine(item))
        })
      }
      //   const test = useSelector((wifiState as any)?.children, state => state)
      //   console.log('111', test)
    }
  })

  return (
    <div className="container-v">
      <div className="box-parent">
        <div className="state-wiff">
          WIFI: {(wifiState as any).hasTag('ON') ? 'ON' : 'OFF'}
          <button
            className="btn btn-primary"
            onClick={() => IphoneService.send('CONNECT')}>
            CONNECT
          </button>
        </div>
        <div className="card-body">
          <div className="card-iphone">
            <div>IPHONE {inphoneState?.context?.userName}</div>

            <div className="chat-view scrollbar">
              {message_iphone.map((item, i) => (
                <div
                  className={'container-chat ' + conterChatClass(item)}
                  key={i}>
                  <p>{item?.message}</p>
                  <span className={item.role == 1 ? 'time-right' : 'time-left'}>
                    {item?.userName}
                  </span>
                </div>
              ))}
              {/* <div className="container-chat">
                <p>Hello. How are you today?</p>
                <span className="time-right">A</span>
              </div>
              <div className="container-chat darker">
                <p>Hey! Im fine. Thanks for asking!</p>
                <span className="time-left">B</span>
              </div> */}
            </div>
            <div className="chat-box">
              <div className="form-chat">
                <input
                  value={inphoneState?.context?.message}
                  onChange={e =>
                    IphoneService.send({
                      type: 'CACHE_INPUT',
                      value: e?.target?.value,
                    })
                  }
                />{' '}
                <button
                  className="btn btn-primary"
                  onClick={() => IphoneService.send('POST')}>
                  Gửi
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CHAT
