import EmoloyeeModal from './employee.modal'
import { useActor, useMachine } from '@xstate/react'
import { useHistory } from 'react-router'
import { employeeMachine } from '_employee.machine/employee.machine'
import { useEffect, useState } from 'react'

// eslint-disable-next-line react/prop-types
const Emoloyee = ({ machine }): JSX.Element => {
  const [state, send] = machine ? (useActor(machine) as any) : useState(null)
  const SUBMIT = e => {
    send({ type: e })
  }
  return (
    <div className="form">
      {/* {JSON.stringify(state?.context)} - {JSON.stringify(state.value)} */}
      {/* <button className='btn btn-primary float-right' onClick={() =>setShowFormAdd(true)}>Tạo Mới </button> */}
      <button
        type="button"
        className="btn btn-primary float-right"
        onClick={() => {
          send('TOGGLE')
        }}>
        Tạo Mới
      </button>
      <div>
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Name</th>
              <th scope="col">Old</th>
              <th scope="col">Phone</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            {state?.context?.data?.map((item, index) => {
              return (
                <tr key={index}>
                  <th scope="row">{item.id}</th>
                  <td>{item.name}</td>
                  <td>{item.old}</td>
                  <td>{item.phone}</td>
                  <td>
                    <button
                      type="button"
                      className="btn btn-primary text-center ml-2"
                      onClick={() => {
                        send({ type: 'TOGGLE', employee: item })
                      }}>
                      Sửa
                    </button>
                    <button
                      type="button"
                      className="btn btn-danger text-center "
                      onClick={() => {
                        send({ type: 'DELETE', id: item.id })
                      }}>
                      Xóa
                    </button>
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
      <EmoloyeeModal
        send={send}
        state={state}
        onSubmit={event => SUBMIT(event)}
        show={state?.hasTag('show')}
        onHide={() => send('TOGGLE')}
      />
    </div>
  )
}

export default Emoloyee
