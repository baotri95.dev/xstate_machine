import { eventType } from '_employee.machine/types'
import { Modal, Form } from 'react-bootstrap'
import { employeeModelConfig } from '_employee.machine/model/empolyee.config'
import { EventFrom } from 'xstate'
type EmployeeEvent = EventFrom<typeof employeeModelConfig>

type IPROPS = {
  show: boolean
  onSubmit: any
  onHide: any
  send: (event: EmployeeEvent) => { event }
  state: any
}
const EmoloyeeModal = (props: IPROPS) => {
  const { show, onSubmit, onHide, send, state } = props
  const data = state?.context
  return (
    <Modal show={show} onHide={onHide}>
      <Modal.Header closeButton>
        <Modal.Title>
          {data?.employee?.id ? 'Cập Nhật Nhân Viên' : 'Tạo Nhân Viên Mới'}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form id="create-new-service-form">
          <Form.Group className="form-group" controlId="userName">
            <Form.Label>Tên</Form.Label>
            <input
              className="form-control"
              // eslint-disable-next-line react/prop-types
              value={state?.context?.employee?.name}
              onChange={e =>
                send({ type: 'CACHE_NAME', value: e.target.value })
              }
            />
          </Form.Group>
          <Form.Group className="form-group" controlId="userName">
            <Form.Label>Sinh nhật</Form.Label>
            <input
              className="form-control"
              // eslint-disable-next-line react/prop-types
              value={state?.context?.employee?.old}
              onChange={e => send({ type: 'CACHE_OLD', value: e.target.value })}
            />
          </Form.Group>
          <Form.Group className="form-group" controlId="userName">
            <Form.Label>SĐT</Form.Label>
            <input
              className="form-control"
              // eslint-disable-next-line react/prop-types
              value={state?.context?.employee?.phone}
              onChange={e =>
                send({ type: 'CACHE_PHONE', value: e.target.value })
              }
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <button className="btn btn-danger" onClick={() => onHide()}>
          Hủy
        </button>
        {data?.employee?.id ? (
          <button
            className="btn btn-primary"
            onClick={() => onSubmit('UPDATE')}>
            Lưu
          </button>
        ) : (
          <button
            className="btn btn-primary"
            onClick={() => onSubmit('CREATE')}>
            Tạo
          </button>
        )}
      </Modal.Footer>
    </Modal>
  )
}

export default EmoloyeeModal
