import { XACTIONS_TYPE } from '_employee.machine/model/actions.interface'
import { employeeModelConfig } from '_employee.machine/model/empolyee.config'
import { eventType } from '_employee.machine/types/index'
import { EventFrom } from 'xstate'

type EmployeeEvent = EventFrom<typeof employeeModelConfig>

const employeeActions: XACTIONS_TYPE = {
  ASSIGN_NAME: employeeModelConfig.assign(
    {
      employee: (ctx, event) => ({ ...ctx.employee, name: event.value }),
    },
    'CACHE_NAME'
  ),
  ASSIGN_OLD: employeeModelConfig.assign(
    {
      employee: (ctx, event) => ({ ...ctx.employee, old: event.value }),
    },
    'CACHE_OLD'
  ),
  ASSIGN_PHONE: employeeModelConfig.assign(
    {
      employee: (ctx, event) => ({ ...ctx.employee, phone: event.value }),
    },
    'CACHE_PHONE'
  ),
  ASSIGN_DATA: employeeModelConfig.assign(
    {
      data: (ctx, event) => event.data,
      employee: { name: '', old: '', phone: '' },
    },
    'CACHE_DATA'
  ),
  RESET_ALL: employeeModelConfig.assign({
    employee: { name: '', old: '', phone: '' },
    success: {},
    error: {},
  }),
  RESET_ERROR: employeeModelConfig.assign({
    error: {},
  }),
  SEND_CREATE: { type: 'xstate.send', event: { type: eventType.CREATE } },
  SEND_UPDATE: { type: 'xstate.send', event: { type: eventType.UPDATE } },
  COPY_DATA: employeeModelConfig.assign(
    {
      employee: (_, event) =>
        event?.employee || { name: '', old: '', phone: '' },
      success: {},
      error: {},
    },
    'TOGGLE'
  ),
  ASSIGN_ID: employeeModelConfig.assign(
    {
      employee: (_, event) => ({ id: event.id, ..._.employee }),
      success: {},
      error: {},
    },
    'DELETE'
  ),
  TOAST_SUCCESS: employeeModelConfig.assign({
    success: (context, event) => {
      const payload = {
        show: true,
        shortText: 'Thành công !',
        longText: event?.data?.message,
        type: 'toast-success',
        position: 'top-right',
      }
      return {
        toast: payload,
      }
    },
  }),
  TOAST_ERROR: employeeModelConfig.assign({
    error: (context, event) => {
      const payload = {
        show: true,
        shortText: 'Lỗi hệ thống',
        longText: event?.data?.message,
        type: 'toast-error',
        position: 'top-right',
      }
      return {
        data: event?.data,
        toast: payload,
      }
    },
  }),
}

export { employeeActions }
