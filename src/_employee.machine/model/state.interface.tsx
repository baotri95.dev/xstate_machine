type STATE = {
  initial?: any
  id?: any
  states?: STATE
  tags?: any
  invoke?: any
  on?: any
  onDone?: any
  onError?: any
  entry?: any
  onEntry?: any
  exit?: any
  after?: any
  meta?: any
}
export interface EmployeeStateInterFace {
  loading: STATE
  reading: STATE
  editing: STATE
  creating: STATE
  deleting: STATE
  invalid?: STATE
  valid: STATE // middleware
  success: STATE
  failure: STATE
  updating: STATE
  notification?: STATE
  done: any
}
export type XSTATE = STATE
