import { Employee, XEmployeeModel } from './employee.interface'
import { XEVENT } from './events.interface'
import { createModel } from 'xstate/lib/model'

// Create Model
const employeeModel: Employee = {
  name: '',
  old: '',
  phone: '',
}

export const XModel: XEmployeeModel = {
  employee: employeeModel,
  data: [],
  error: {},
  success: {},
}

export const events = {
  CACHE_NAME: value => ({ value }),
  CACHE_OLD: value => ({ value }),
  CACHE_PHONE: value => ({ value }),
  CACHE_DATA: data => ({ data }),
  CREATE: () => ({}),
  UPDATE: () => ({}),
  TOGGLE: employee => ({ employee }),
  RETRY: () => ({}),
  DELETE: id => ({ id }),
  DELAY_SUCCESS: () => ({}),
  DELAY_FAILURE: () => ({}),
  TO_SUCCESS: () => ({}),
  TO_FAILURE: () => ({}),
}
export const employeeModelConfig = createModel(XModel, {
  events,
})
