export interface XACTIONS_TYPE {
  ASSIGN_NAME: any
  ASSIGN_OLD: any
  ASSIGN_PHONE: any
  ASSIGN_DATA: any
  RESET_ALL: any
  RESET_ERROR: any
  SEND_CREATE: any
  SEND_UPDATE: any
  ASSIGN_ID: any
  COPY_DATA: any
  TOAST_SUCCESS: any
  TOAST_ERROR: any
}
