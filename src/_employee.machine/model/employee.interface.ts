export interface Employee {
  name: string
  old: string
  phone: string
  id?: string
}

export interface XEmployeeModel {
  employee?: Employee
  data?: any[]
  error?: any
  success: any
}
