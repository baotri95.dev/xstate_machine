import { Employee } from './employee.interface'

/* eslint-disable @typescript-eslint/ban-types */
export interface XEVENT_TYPE {
  CACHE_NAME?: EVENT
  CACHE_OLD?: EVENT
  CACHE_PHONE?: EVENT
  CREATE?: EVENT
  UPDATE?: EVENT
  TOGGLE?: EVENT
  DELETE?: EVENT
  RESET?: EVENT
}

type EVENT = {
  cond?: any
  type?: any
  actions?: any[] | any
  target?: any
}
export interface XEVENT {
  CACHE_NAME: (value: string) => { value }
  CACHE_OLD: (value: string) => { value }
  CACHE_PHONE: (value: string) => { value }
  CACHE_DATA: (data: Employee[]) => { data }
  CREATE: () => {}
  UPDATE?: () => {}
  DELETE: (id) => { id }
  TOGGLE?: (employee: Employee) => { employee }
  RETRY?: () => {}
  RESET?: () => {}
  DELAY_SUCCESS: () => {}
  DELAY_FAILURE: () => {}
  TO_FAILURE: () => {}
  TO_SUCCESS: () => {}
}
