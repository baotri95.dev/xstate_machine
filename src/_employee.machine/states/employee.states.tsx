import { employeeModelConfig } from '_employee.machine/model/empolyee.config'
import { actionType, eventType, stateType } from '_employee.machine/types'
import { send } from 'xstate'
import {
  EmployeeStateInterFace,
  XSTATE,
} from '_employee.machine/model/state.interface'

// config state
const loading: XSTATE = {
  invoke: {
    src: 'getData',
    onDone: {
      target: stateType.reading,
      actions: actionType.ASSIGN_DATA,
    },
    onError: {
      target: stateType.failure,
    },
  },
}
const reading: XSTATE = {
  on: {
    TOGGLE: {
      target: stateType.editing,
      actions: [actionType.COPY_DATA],
    },
    DELETE: {
      target: stateType.deleting,
      actions: actionType.ASSIGN_ID,
    },
  },
}
const editing: XSTATE = {
  tags: ['show'],
  on: {
    CACHE_NAME: {
      actions: actionType.ASSIGN_NAME,
    },
    CACHE_OLD: {
      actions: actionType.ASSIGN_OLD,
    },
    CACHE_PHONE: {
      actions: actionType.ASSIGN_PHONE,
    },
    CREATE: {
      target: stateType.valid,
      actions: actionType.SEND_CREATE,
    },
    UPDATE: {
      target: stateType.valid,
      actions: actionType.SEND_UPDATE,
    },
    TOGGLE: {
      //hide popup
      target: stateType.reading,
      actions: [actionType.RESET_ERROR],
    },
  },
}
const creating: XSTATE = {
  invoke: {
    src: 'createEmployee',
    onDone: {
      target: stateType.notification,
      actions: [
        actionType.TOAST_SUCCESS,
        {
          type: 'xstate.send',
          event: { type: eventType.DELAY_SUCCESS },
        },
      ],
    },
    onError: {
      target: stateType.notification,
      actions: [
        actionType.TOAST_ERROR,
        {
          type: 'xstate.send',
          event: { type: eventType.DELAY_FAILURE },
        },
      ],
    },
  },
}
const updating: XSTATE = {
  invoke: {
    src: 'updateEmployee',
    onDone: {
      target: stateType.notification,
      actions: [
        actionType.TOAST_SUCCESS,
        {
          type: 'xstate.send',
          event: { type: eventType.DELAY_SUCCESS },
        },
      ],
    },
    onError: {
      target: stateType.notification,
      actions: [
        actionType.TOAST_ERROR,
        {
          type: 'xstate.send',
          event: { type: eventType.DELAY_SUCCESS },
        },
      ],
    },
  },
}
const valid: XSTATE = {
  id: 'valid',
  on: {
    CREATE: {
      target: stateType.creating,
    },
    UPDATE: {
      target: stateType.updating,
    },
  },
}
const success: XSTATE = {
  on: {
    TOGGLE: {
      target: stateType.editing,
      actions: actionType.COPY_DATA,
    },
    DELETE: {
      target: stateType.deleting,
      actions: actionType.ASSIGN_ID,
    },
  },
}
const failure: XSTATE = {
  on: {
    CACHE_NAME: {
      actions: actionType.ASSIGN_NAME,
    },
    CACHE_OLD: {
      actions: actionType.ASSIGN_OLD,
    },
    CACHE_PHONE: {
      actions: actionType.ASSIGN_PHONE,
    },
    CREATE: {
      target: stateType.creating,
    },
    UPDATE: {
      target: stateType.updating,
    },
    TOGGLE: {
      target: stateType.reading,
      actions: [
        actionType.RESET_ERROR,
        {
          type: 'xstate.send',
          event: { type: eventType.TOGGLE },
        },
      ],
    },
    DELETE: {
      target: stateType.deleting,
      actions: actionType.ASSIGN_ID,
    },
  },
}
const deleting: XSTATE = {
  invoke: {
    src: 'deleteEmpolyee',
    onDone: {
      target: stateType.notification,
      actions: [
        actionType.TOAST_SUCCESS,
        {
          type: 'xstate.send',
          event: { type: eventType.DELAY_SUCCESS },
        },
      ],
    },
    onError: {
      target: stateType.notification,
      actions: [
        actionType.TOAST_ERROR,
        {
          type: 'xstate.send',
          event: { type: eventType.DELAY_FAILURE },
        },
      ],
    },
  },
}
const notification: XSTATE = {
  on: {
    DELAY_SUCCESS: {
      actions: [
        // show toast 4s then to state succsess by TO_SUCCESS event
        send({ type: eventType.TO_SUCCESS }, { delay: 4000 }),
      ],
    },
    DELAY_FAILURE: {
      actions: [send({ type: eventType.TO_FAILURE }, { delay: 4000 })],
    },
    TO_SUCCESS: {
      target: stateType.success,
      actions: [actionType.RESET_ALL, actionType.RESET_ERROR],
    },
    TO_FAILURE: {
      target: stateType.failure,
      actions: actionType.RESET_ERROR,
    },
  },
}
export const EmployeeState: EmployeeStateInterFace = {
  loading,
  reading,
  editing,
  creating,
  updating,
  invalid: {},
  valid,
  success,
  failure,
  deleting,
  notification,
  done: {
    type: 'final',
  },
}
