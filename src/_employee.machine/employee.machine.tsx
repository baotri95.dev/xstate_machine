import { employeeActions } from './actions/empoyee.actions'
import { XInvokeServices } from './invoke'
import { employeeModelConfig } from './model/empolyee.config'
import { EmployeeState } from './states/employee.states'
import { stateType } from './types'
import { interpret } from 'xstate'
import { inspect } from '@xstate/inspect'
import { useMachine } from '@xstate/react'

// state machine
const states = {
  ...EmployeeState,
}
// action
const actions = {
  ...employeeActions,
}
// event
const services = {
  ...XInvokeServices,
}
// config
const machineConfig = {
  id: 'employeeRef',
  initial: stateType.loading,
  context: employeeModelConfig.initialContext,
  states,
  // on: {
  //   DONE: {
  //     target: '#machine-.done',
  //   },
  // },
}
// create machine
export const employeeMachine = employeeModelConfig.createMachine(
  machineConfig,
  {
    actions,
    services,
  }
)

// OPENT FLOW CHART STATE
inspect({
  url: 'https://statecharts.io/inspect',
  iframe: false,
})
