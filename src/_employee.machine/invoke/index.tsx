import { XEmployeeModel } from '_employee.machine/model/employee.interface'
import {
  FetchEmployee,
  createEmployee,
  updateEmployee,
  deleteEmpolyee,
} from 'pages/services/empolyee.services'

export const XInvokeServices = {
  getData: () => FetchEmployee(),
  createEmployee: (ctx: XEmployeeModel, event) => createEmployee(ctx.employee),
  updateEmployee: (ctx: XEmployeeModel) => updateEmployee(ctx.employee),
  deleteEmpolyee: (ctx: XEmployeeModel) => deleteEmpolyee(ctx.employee.id),
}
