import i18n from 'i18next'
import { translation as translation_vi } from 'locale/vi'
import { translation as translation_en } from 'locale/en'
import { initReactI18next } from 'react-i18next'

// the translations
// (tip move them in a JSON file and import them)
const resources = {
  en: {
    translation: translation_en,
  },
  vi: {
    translation: translation_vi,
  },
} 

const defaultLang = localStorage.getItem('lang')
const fallbackLng = defaultLang ? defaultLang : 'vi-VN'
i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    fallbackLng: fallbackLng,
    keySeparator: false,
    interpolation: {
      escapeValue: false,
    },
  })

export default i18n
