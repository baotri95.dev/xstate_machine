import { createMachine, assign, spawn, send, sendParent, actions } from 'xstate'
import { interpret } from 'xstate'
import { inspect } from '@xstate/inspect'
const { choose, log } = actions

const ATM_Machine = createMachine({
  id: 'ATM-SG-QUAN-1',
  initial: 'open',
  context: {
    password: null,
    code_data: null,
    errorMess: '',
    errCount: 0,
    block: 0,
    result: {},
    withdrawal_amount: 0,
    message: '',
  },
  states: {
    open: {
      on: {
        check: [{ target: 'checking' }],
      },
    },
    checking: {
      invoke: {
        src: (ctx, event: any) => (callback, onReceive) => {
          if (event.data.STATE == 'INACTIVE') {
            callback({ type: 'ON_ERROR', message: 'Thẻ hết hạn !' })
            return
          }
          callback({
            type: 'ON_SUCCESS',
            code: event.data.CODE,
          })
        },
        onDone: {},
      },
      on: {
        ON_ERROR: {
          actions: sendParent((_ctx, event: any) => ({
            type: 'NOTI',
            message: event.message,
          })),
        },
        ON_SUCCESS: {
          actions: [
            sendParent((_ctx, event: any) => ({
              type: 'NOTI',
              message: 'Thẻ có thể sử dụng',
            })),
            assign({
              code_data: (_, event: any) => {
                console.log(event)
                return event.code
              },
            }),
          ],
        },
        TO_AUTH: 'auth',
      },
    },
    auth: {
      on: {
        INPUT_PASSWORD: {
          actions: [
            assign({
              password: (ctx, event: any) => {
                return event.value
              },
            }),
          ],
        },
        ENTER: [
          {
            cond: ctx => {
              return ctx.code_data == ctx.password
            },
            actions: assign({
              chuyentien: (ctx, event: any) => true,
              trasodu: (ctx, event: any) => true,
              naptien: (ctx, event: any) => false,
              ruttien: (ctx, event: any) => true,
            }),
            target: 'menu',
          },
          {
            id: 'i',
            cond: ctx => {
              return ctx.code_data != ctx.password
            },
            actions: assign({
              errorMess: (ctx: any) => 'Nhập sai' + ctx.errCount + 1,
            }),
            target: 'falid',
          },
        ],
      },
    },
    menu: {
      on: {
        TRASODU: {
          target: 'tra_so_du',
          actions: [sendParent({ type: 'CHECK_SO_DU', status: 'view' })],
        },
        RUTTIEN: {
          target: 'rut_tien',
          actions: [
            assign({
              result: _ => null,
              message: _ => '',
            }),
          ],
          // actions: [sendParent({ type: 'CHECK_SO_DU', status: 'withdraw' })],
        },
      },
    },
    tra_so_du: {
      on: {
        ON_DONE: {
          target: 'result',
          actions: assign({
            result: (_, event: any) => event.value,
          }),
        },
      },
    },
    rut_tien: {
      on: {
        INPUT_MONEY: {
          actions: assign({
            withdrawal_amount: (ctx, event: any) => event?.value,
          }),
        },
        ENTER: [
          {
            actions: [
              sendParent((_: any) => ({
                type: 'CHECK_SO_DU',
                status: 'withdraw',
                value: _.withdrawal_amount,
              })),
            ],
          },
          {},
        ],
        ON_ERROR: {
          target: 'result',
          actions: assign({ errorMess: (_, event: any) => event?.message }),
        },
        ON_DONE: {
          target: 'pending',
          actions: assign({
            result: (_, event: any) => event.value,
            message: (_, event: any) => event.message,
          }),
        },
      },
    },
    pending: {
      entry: { type: 'xstate.send', event: { type: 'PAY' } },
      on: {
        PAY: [
          {
            target: 'result',
            actions: [
              sendParent((ctx: any) => ({
                type: 'CHECK_SO_DU',
                status: 'refresh',
                MONEY: ctx.result.MONEY - ctx.withdrawal_amount,
              })),
              assign({
                errorMess: _ => '',
                message: 'Thanh toán thành công',
              }),
            ],
          },
        ],
      },
    },
    result: {
      on: {
        BACK_MENU: {
          target: 'menu',
        },
        OUT: {
          target: 'out_card',
          actions: [
            assign({
              result: (_, event: any) => null,
              withdrawal_amount: (_, event: any) => 0,
              password: (_, event: any) => null,
              code: (_, event: any) => null,
              message: (_, event: any) => '',
              errorMess: _ => '',
            }),
            {
              type: 'xstate.send',
              event: { type: 'OUT', status: 'FINISHED' },
            },
          ],
        },
      },
    },
    falid: {
      entry: [send({ type: 'RETRY', delay: 1000 })],
      on: {
        RETRY: [
          {
            target: 'auth',
            cond: ctx => {
              const b = 2
              return ctx.errCount < b
            },
            actions: assign({
              errCount: (_: any) => _.errCount + 1,
            }),
          },
          {
            target: 'block',
            cond: ctx => {
              const b = 2
              return ctx.errCount == b
            },
            actions: assign({
              errorMess: (_: any) => 'Tài khoản tạm thời bị khóa ',
            }),
          },
        ],
      },
    },
    block: {
      after: {
        5000: [
          {
            target: 'auth',
            cond: ctx => {
              const maxBlock = 2
              return ctx.block < maxBlock
            },
            actions: assign({
              errCount: 0,
              block: (_: any) => _.block + 1,
              errorMess: '',
            }),
          },
          {
            target: 'out_card',
            cond: ctx => {
              const maxBlock = 2
              return ctx.errCount == maxBlock
            },
            actions: [
              assign({
                errCount: () => 0,
                block: (_: any) => 0,
                code_data: '',
                errorMess: () => 'Tài khoản của bị khóa 30 ngày',
              }),
              { type: 'xstate.send', event: { type: 'OUT', status: 'BLOCK' } },
            ],
          },
        ],
      },
    },
    out_card: {
      on: {
        OUT: [
          {
            cond: (_, event: any) => event.status == 'BLOCK',
            actions: sendParent({
              type: 'DONE',
              STATE: 'INACTIVE',
              ISBLOCK: 1,
            }),
          },
          {
            cond: (_, event: any) => event.status == 'FINISHED',
            actions: sendParent({
              type: 'DONE',
              STATE: 'ACTIVE',
              ISBLOCK: 0,
            }),
          },
        ],
      },
    },
  },
})
const CARD = createMachine({
  initial: 'ide',
  context: {
    NUMBER_CARD: '9779-0990-1222',
    STATE: 'ACTIVE',
    MONEY: 200000,
    USER: 'DO BAO TRI',
    message: null,
    CODE: 44096100,
    ISBLOCK: 0,
  },
  states: {
    ide: {
      on: {
        PUT: { target: 'waiting' },
      },
    },
    waiting: {
      invoke: {
        id: 'ATM-QUAN-1',
        src: ATM_Machine,
      },
      entry: send(ctx => ({ type: 'check', data: ctx }), {
        to: 'ATM-QUAN-1',
      }),
      on: {
        NOTI: [
          {
            cond: ctx => ctx.STATE == 'ACTIVE',
            actions: [
              assign({ message: (ctx, event: any) => event?.message }),
              send({ type: 'TO_AUTH' }, { to: 'ATM-QUAN-1' }),
            ],
          },
        ],
        CHECK_SO_DU: [
          {
            cond: (ctx, event: any) => event.status == 'view',
            actions: send(
              (ctx: any) => ({
                type: 'ON_DONE',
                value: { ...ctx },
              }),
              { to: 'ATM-QUAN-1' }
            ),
          },
          {
            cond: (ctx, event: any) =>
              event.status == 'withdraw' && event.value <= ctx.MONEY,
            actions: send(
              (ctx: any) => ({
                type: 'ON_DONE',
                value: { ...ctx },
                message: 'Tiến hành giao dịch',
              }),
              { to: 'ATM-QUAN-1' }
            ),
          },
          {
            cond: (ctx, event: any) =>
              event.status == 'withdraw' && event.value > ctx.MONEY,
            actions: send(
              (ctx: any) => ({
                type: 'ON_ERROR',
                value: { ...ctx },
                message: 'Tài khoản không đủ để giao dịch',
              }),
              { to: 'ATM-QUAN-1' }
            ),
          },
          {
            cond: (ctx, event: any) => event.status == 'refresh',
            actions: [
              send(
                (ctx: any) => ({
                  type: 'ON_DONE',
                  value: { ...ctx },
                  message: 'Tài khoản không đủ để giao dịch',
                }),
                { to: 'ATM-QUAN-1' }
              ),
              assign({
                MONEY: (_, event: any) => event.MONEY,
              }),
            ],
          },
        ],
        DONE: {
          target: 'done',
          actions: [
            assign({
              STATE: (ctx: any, event: any) =>
                event?.STATE ? event?.STATE : ctx.STATE,
              ISBLOCK: (ctx: any, event: any) =>
                event?.ISBLOCK ? event?.ISBLOCK : ctx.ISBLOCK,
              message: (_, event: any) =>
                event?.ISBLOCK ? 'Thẻ đã bị khóa 30 ngày' : '',
            }),
          ],
        },
      },
    },
    done: { type: 'final' },
  },
  id: 'CARD VCB',
})

// OPENT FLOW CHART STATE
inspect({
  url: 'https://statecharts.io/inspect',
  iframe: false,
})

// Global Service

export const CARDService = interpret(CARD, {
  devTools: true,
}).start()
