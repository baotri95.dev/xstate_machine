import { createMachine, assign, spawn, send, sendParent } from 'xstate'
import { interpret } from 'xstate'
import { inspect } from '@xstate/inspect'
import context from 'react-bootstrap/esm/AccordionContext'

const IPHONE_CHAT_MACHINE = createMachine({
  id: 'iphone',
  initial: 'ide',
  context: {
    LIST_MESSAGE: [],
    userName: 'baotriDev.95',
    message: '',
    role: 0,
    iphoneMachine: null,
  },
  states: {
    ide: {
      entry: assign({
        iphoneMachine: () => spawn(NETWORK, 'network'),
      }),
      on: {
        CONNECT: {
          actions: send({ type: 'SEND_CONNECT' }, { to: 'network' }),
        },
        CONNECTED: {
          target: 'success',
          actions: { type: 'xstate.send', event: { type: 'READY_CHAT' } },
        },
        RECEIVE: {
          target: 'notifi',
        },
      },
    },
    success: {
      on: {
        READY_CHAT: {
          target: 'chat',
        },
      },
    },
    chat: {
      on: {
        CACHE_USERNAME: {
          actions: assign({
            userName: (ctx, event: any) => event.value,
          }),
        },
        CACHE_INPUT: {
          actions: assign({
            message: (ctx, event: any) => event.value,
          }),
        },
        POST: {
          actions: send(
            (context: any, event: any) => ({
              type: 'SEND_POST',
              data: {
                userName: context.userName,
                message: context.message,
                role: context.role,
              },
            }),
            { to: 'network' }
          ),
        },
        RECEIVE: 'notifi',
      },
    },
    notifi: {
      entry: [
        (ctx, event: any) => {
          ctx.LIST_MESSAGE.push({
            userName: event.userName,
            message: event.message,
            role: event.role ? event.role : ctx.role,
          })
          console.log(ctx, event)
        },
        assign({ message: '' }),
      ],
      after: {
        2000: {
          target: 'chat',
        },
      },
    },
  },
})

// đảm nhiệm BE ( send, data)
const NETWORK = createMachine({
  id: 'net_work',
  initial: 'disconnect',
  context: {
    data: { userName: '', message: '', role: 0 },
    client: null,
  },
  states: {
    disconnect: {
      on: {
        SEND_CONNECT: 'connected',
      },
    },
    connected: {
      tags: ['ON'],
      after: {
        2000: {
          actions: sendParent({ type: 'CONNECTED' }),
        },
      },
      on: {
        SEND_POST: {
          target: 'chatbox',
          actions: [
            assign({
              data: (_, event: any) => event.data,
            }),
            {
              type: 'xstate.send',
              event: { type: 'TO_PARENT' },
            },
            assign({
              client: (ctx: any) =>
                !ctx.client
                  ? spawn(SAMSUNG_CHAT_MACHINE, 'client')
                  : ctx.client,
            }),
          ],
        },
        REPLY: {
          target: 'chatbox',
          actions: [
            assign({
              data: (_, event: any) => event.data,
            }),
            {
              type: 'xstate.send',
              event: { type: 'TO_MESS' },
            },
          ],
        },
      },
    },
    chatbox: {
      tags: ['ON'],
      on: {
        TO_MESS: {
          target: 'waiting',
          actions: [
            sendParent((ctx: any) => ({
              type: 'RECEIVE',
              userName: ctx?.data?.userName,
              message: ctx?.data?.message,
              role: 1,
            })),
            send((ctx: any) => ({
              type: 'TO_CHILD',
              userName: ctx?.data?.userName,
              message: ctx?.data?.message,
              role: 0,
            })),
          ],
        },
        TO_PARENT: {
          target: 'waiting',
          actions: [
            sendParent((ctx: any) => ({
              type: 'RECEIVE',
              userName: ctx?.data?.userName,
              message: ctx?.data?.message,
              role: ctx?.data?.role,
            })),
            send((ctx: any) => ({
              type: 'TO_CHILD',
              userName: ctx?.data?.userName,
              message: ctx?.data?.message,
              role: 1,
            })),
          ],
        },
      },
    },
    waiting: {
      tags: ['ON'],
      on: {
        TO_CHILD: {
          actions: [
            send(
              (ctx: any, event: any) => ({
                type: 'RECEIVE',
                userName: ctx?.data?.userName,
                message: ctx?.data?.message,
                role: event.role,
              }),
              {
                to: 'client',
              }
            ),
          ],
        },
      },
      after: {
        1000: {
          target: 'connected',
        },
      },
    },
  },
})
const SAMSUNG_CHAT_MACHINE = createMachine({
  id: 'client',
  initial: 'ide',
  context: {
    LIST_MESSAGE: [],
    userName: 'khoa123.95',
    message: '',
    role: 0,
    samsungMachine: null,
  },
  states: {
    ide: {
      on: {
        RECEIVE: {
          target: 'notifi',
        },
      },
    },
    success: {
      on: {
        READY_CHAT: {
          target: 'chat',
        },
      },
    },
    chat: {
      on: {
        CACHE_USERNAME: {
          actions: assign({
            userName: (ctx, event: any) => event.value,
          }),
        },
        CACHE_INPUT: {
          actions: assign({
            message: (ctx, event: any) => event.value,
          }),
        },
        POST: {
          actions: sendParent((context: any) => ({
            type: 'REPLY',
            data: {
              userName: context.userName,
              message: context.message,
              role: 0,
            },
          })),
        },
        RECEIVE: {
          target: 'notifi',
        },
      },
    },
    notifi: {
      entry: [
        (ctx, event: any) => {
          ctx.LIST_MESSAGE.push({
            userName: event.userName,
            message: event.message,
            role: event.role,
          })
          console.log(ctx, event)
        },
        assign({ message: '' }),
      ],
      after: {
        2000: 'chat',
      },
    },
  },
})
// OPENT FLOW CHART STATE
inspect({
  url: 'https://statecharts.io/inspect',
  iframe: false,
})

// Global Service

export const IphoneService = interpret(IPHONE_CHAT_MACHINE, {
  devTools: true,
}).start()
