import { createModel } from 'xstate/lib/model'

export function mergeMeta(meta) {
  return Object.keys(meta).reduce((acc, key) => {
    const value = meta[key]
    // Assuming each meta value is an object
    Object.assign(acc, value)

    return acc
  }, {})
}
export const xPath = meta =>
  (mergeMeta(meta ? meta : { path: '/login' }) as any).path

export const xmodel = createModel(
  {
    username: '',
    password: '',
    history,
    home: {
      data: [1223311],
    },
    dashboardRef: null,
    employeeRef: null,
  },
  {
    events: {
      cacheUserName: (value: any) => ({ value }),
      cachePassword: (value: any) => ({ value }),
      SUBMIT: () => ({}),
      goPageHome: () => ({}),
      goPageUser: () => ({}),
      reLoad: () => ({}),
      add: () => ({}),
      delete: () => ({}),
      update: () => ({}),
      search: () => ({}),
      TO_DASHBOARD: () => ({}),
      Logout: () => ({}),
    },
  }
)
export const action = {
  asignUserName: xmodel.assign(
    {
      username: (ctx, event) => {
        return event.value
      },
    },
    'cacheUserName'
  ),
  asignPassWord: xmodel.assign(
    {
      password: (_, event) => event.value,
    },
    'cachePassword'
  ),
  auth: { type: 'xstate.send', event: { type: 'PASS' } },
}
export const guards = {
  isUserEmpty: _ => _.username.length == 0,
  isUserBadFormat: _ => false,
  isUserMaxlength: _ => _.username.length > 20,
  isUserMinlength: _ => _.username.length < 7,
  isPasswordEmpty: _ => _.password.length == 0,
  isPasswordMaxlength: _ => _.password.length > 30,
  isPasswordMinlength: _ => _.password.length < 5,
  checkLoggin: () => {
    const isAuth = JSON.parse(localStorage.getItem('token'))
    return isAuth
  },
}
