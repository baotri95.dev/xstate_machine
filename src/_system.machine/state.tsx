import { xmodel } from './options'
import { assign, createMachine, sendUpdate, spawn } from 'xstate'
import { send, sendParent } from 'xstate/lib/actions'
import { employeeMachine } from '_employee.machine/employee.machine'
export const dashBoardMsachine = createMachine({
  id: 'dashboard',
  tags: ['isAuth'],
  context: {
    employeeRef: null,
    homeData: [],
  },
  entry: send(() => {
    let path = { type: 'GO_HOME' }
    switch (location.pathname) {
      case '/admin/employee':
        {
          path = { type: 'GO_EMPLOYEE' }
        }
        break
      case '/admin/user':
        {
          path = { type: 'GO_USER' }
        }
        break
      default: {
        path = { type: 'GO_HOME' }
      }
    }
    return path
  }),
  states: {
    homePage: {
      initial: 'noError',
      states: {
        noError: {
          initial: 'form1',
          states: {
            form1: {
              on: {
                next: {
                  target: 'form2',
                },
              },
            },
            form2: {},
          },
        },
        error: {},
        end: {},
        historyHome: {
          type: 'history',
          history: 'deep',
        },
      },
      meta: { path: '/admin/home' },
    },
    employeePage: {
      initial: 'noError',
      invoke: {
        id: 'empMachine',
        src: employeeMachine,
      },
      states: {
        noError: {},
        error: {},
      },
      meta: { path: '/admin/employee' },
      on: {},
    },
    userPage: {
      initial: 'noError',
      states: {
        noError: {},
        error: {},
      },
      meta: { path: '/admin/user' },
    },
    chat_iphone: {
      initial: 'noError',
      states: {
        noError: {},
        error: {},
      },
      meta: {
        path: '/admin/chat_iphone',
      },
    },
    chat_samsung: {
      initial: 'noError',
      states: {
        noError: {},
        error: {},
      },
      meta: {
        path: '/admin/chat_samsung',
      },
    },

    logout: {
      meta: {
        path: '/auth/login',
      },
      after: {
        500: { target: 'done' },
      },
    },

    done: {
      type: 'final',
    },
  },
  on: {
    GO_HOME: {
      target: '.homePage',
    },
    GO_USER: {
      target: '.userPage',
    },
    GO_EMPLOYEE: {
      target: '.employeePage',
    },
    GO_CHAT_IPHONE: {
      target: '.chat_iphone',
    },
    GO_CHAT_SAMSUNG: {
      target: '.chat_samsung',
    },
    HISTORY_HOME: {
      target: '.homePage.historyHome',
    },
    LOG_OUT: {
      target: 'logout',
      actions: [
        sendParent({ type: 'Logout' }),
        send({ type: 'DONE' }, { to: (ctx: any) => ctx.employeeRef }),
      ],
    },
  },
})
const userNameState = {
  id: 'user',
  initial: 'noError',
  states: {
    noError: {},
    error: {
      initial: 'isEmpty',
      tags: ['userErr'],
      states: {
        isEmpty: {
          meta: {
            message: 'UserName is empty!',
          },
        },
        maxLength: {
          meta: {
            message: 'UserName enter too 20 character!',
          },
        },
        minLength: {
          meta: {
            message: 'UserName enter less than 8 character!',
          },
        },
      },
    },
  },
}
const passWordState = {
  id: 'pass',
  initial: 'noError',
  states: {
    noError: {},
    error: {
      initial: 'isEmpty',
      tags: ['passwordErr'],

      states: {
        isEmpty: {
          meta: {
            message: 'Password is empty!',
          },
        },
        maxLength: {
          meta: {
            message: 'Password enter too 30 character!',
          },
        },
        minLength: {
          meta: {
            message: 'Password enter less than 8 characters!',
          },
        },
      },
    },
  },
}
export const loginStates = {
  id: 'login_fr2',
  initial: 'ready',
  context: xmodel.initialContext,
  states: {
    ready: {
      meta: {
        path: '/auth/login',
      },
      entry: 'auth',
      on: {
        cacheUserName: {
          actions: 'asignUserName',
          target: '.user.noError',
        },
        cachePassword: {
          actions: 'asignPassWord',
          target: '.password.noError',
        },
        SUBMIT: [
          {
            cond: 'isUserEmpty',
            target: '.user.error.isEmpty',
          },
          {
            cond: 'isUserMaxlength',
            target: '.user.error.maxLength',
          },
          {
            cond: 'isUserMinlength',
            target: '.user.error.minLength',
          },
          {
            cond: 'isPasswordEmpty',
            target: '.password.error.isEmpty',
          },
          {
            cond: 'isPasswordMaxlength',
            target: '.password.error.maxLength',
          },
          {
            cond: 'isPasswordMinlength',
            target: '.password.error.minLength',
          },
          {
            target: 'waiting',
          },
        ],
        PASS: {
          cond: 'checkLoggin',
          target: 'dashBoard',
        },
      },
      states: {
        user: {
          ...userNameState,
        },
        password: {
          ...passWordState,
        },
        serviceError: {
          meta: {
            message: 'Lỗi đăng nhập!',
          },
        },
      },
    },
    waiting: {
      invoke: {
        src: 'loggin',
        onDone: {
          actions: [
            (ctx, event) => {
              localStorage.setItem('token', JSON.stringify(event.data.isAuth))
            },
            { type: 'xstate.send', event: { type: 'TO_DASHBOARD' } },
          ],
          target: 'dashBoard',
        },
        onError: [
          {
            target: 'ready.serviceError',
            // actions: assign({
            //   error: () {
            //     toast: {
            //       show: true,
            //       shortText: 'Lỗi hệ thống',
            //       longText: event?.data?.message,
            //       type: 'toast-error',
            //       position: 'top-right',
            //     },
            //   },
            // }),
          },
        ],
      },
    },
    dashBoard: {
      tags: ['isAuth'],
      entry: 'connect_dashboard',
      on: {
        Logout: {
          target: 'ready',
          actions: () => localStorage.removeItem('token'),
        },
        TO_DASHBOARD: {
          actions: [
            send({ type: 'GO_HOME' }, { to: (ctx: any) => ctx.dashboardRef }),
          ],
        },
      },
    },
  },
}
