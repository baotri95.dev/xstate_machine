import { inspect } from '@xstate/inspect'
import { useActor } from '@xstate/react'
import { interpret, spawn } from 'xstate'
import { action, guards, xmodel } from './options'
import { dashBoardMsachine, loginStates } from './state'

const actions: any = {
  ...action,
  connect_dashboard: xmodel.assign({
    dashboardRef: () => spawn(dashBoardMsachine, { sync: false }),
  }),
}
export const login_Machine = xmodel.createMachine(loginStates, {
  actions,
  guards,
  services: {
    loggin: ctx => {
      return new Promise((res, rejects) => {
        if (ctx.username == 'baotri95.dev' && ctx.password == '12345678') {
          res({ isAuth: true })
        }
        rejects({ code: 400 })
      })
    },
  },
})
// OPENT FLOW CHART STATE
inspect({
  url: 'https://statecharts.io/inspect',
  iframe: false,
})

// Global Service

export const login_fr2Service = interpret(login_Machine, {
  devTools: true,
})
  .onTransition(item => {
    const children = item.children && Object.values(item.children)[0]
    console.log(' ')
    console.log(' ')
    console.log('/**************************STATE***************************/')
    console.log(
      '\x1b[36mParent State:\x1b[0m \x1b[32m%s\x1b[0m',
      `${JSON.stringify(item.value)}`
    )
    console.log(
      '\x1b[36mClicked/Ran Event:\x1b[0m \x1b[32m%s\x1b[0m',
      item.event.type
    )
    console.log(
      '  \x1b[42mHistory:\x1b[0m \x1b[32m%s\x1b[0m',
      JSON.stringify(item.history ? item?.history?.value : '~Not Yet')
    )
    console.log('\x1b[31mChildren States:\x1b[0m ')
    console.log(
      '  \x1b[42mChild Machine:\x1b[0m \x1b[32m%s\x1b[0m',
      'dashboardRef'
    )
    const childState = children?.getSnapshot()
      ? children?.getSnapshot()
      : ({} as any)
    const childStateValue = children?.getSnapshot()?.value
      ? JSON.stringify(children?.getSnapshot().value)
      : 'disconect'
    console.log(
      '  \x1b[42mChild state:\x1b[0m \x1b[32m%s\x1b[0m',
      childStateValue
    )
    console.log(
      '  \x1b[42mClick/Run Child Event:\x1b[0m \x1b[32m%s\x1b[0m',
      JSON.stringify(childState.event ? childState.event.type : '~Not Yet')
    )
    console.log(
      '  \x1b[42mHistory:\x1b[0m \x1b[32m%s\x1b[0m',
      JSON.stringify(childState ? childState?.history?.value : '~Not Yet')
    )
    console.log(childState)
    console.log('/**************************END***************************/')
  })
  .start()
