import { createMachine, assign, spawn, send, sendParent } from 'xstate'
import { interpret } from 'xstate'
import { inspect } from '@xstate/inspect'

const loginMachine = createMachine(
  {
    initial: 'open',
    context: {
      userName: '',
      passWord: '',
      userNameErr: '',
      passWordErr: '',
    },
    states: {
      open: {
        states: {
          invalid: {
            entry: 'ERROR',
          },
        },
        on: {
          cache_user: {
            actions: 'asign_user',
          },
          cache_password: {
            actions: 'asign_pass',
          },
          submit: [
            {
              cond: 'invald_inut',
              actions: { type: 'xstate.send', event: { type: 'ERROR' } },
              target: '.invalid',
            },
            {
              cond: 'vaild',
              target: 'Done',
            },
          ],
        },
      },
      Done: {},
    },
    id: 'login',
  },
  {
    actions: {
      asign_user: assign({
        userName: (ctx, event: any) => event.value,
      }),
      asign_pass: assign({
        passWord: (ctx, event: any) => event.value,
      }),
      ERROR: assign({
        userNameErr: ctx => {
          console.log(ctx)
          if (!ctx?.userName) {
            return 'UserName không được bỏ trống'
          } else if (ctx.userName.length < 10) {
            return 'User name nhập hơn 10 ký tự'
          } else return ''
        },
        passWordErr: ctx =>
          !ctx.passWord ? 'Password không được bỏ trống' : '',
      }),
    },
    guards: {
      invald_inut: ctx => {
        return !ctx.passWord || !ctx.userName || ctx.userName.length < 10
      },
      vaild: ctx => {
        return ctx.passWord && ctx.userName ? true : false
      },
    },
  }
)

// OPENT FLOW CHART STATE
inspect({
  url: 'https://statecharts.io/inspect',
  iframe: false,
})

// Global Service

export const loginService = interpret(loginMachine, {
  devTools: true,
})
