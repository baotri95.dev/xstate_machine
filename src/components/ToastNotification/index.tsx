import { ReactComponent as CheckIcon } from 'assets/icons/checkbox-circle-fill.svg'
import { ReactComponent as CloseIcon } from 'assets/icons/close-circle-fill.svg'
import Toast from 'react-bootstrap/Toast'
import { useActor } from '@xstate/react'
import { useEffect } from 'react'

function ToastNotification(props: any): JSX.Element {
  const [state, send] = useActor(props.machine) as any
  const toast = Object.values((state as any).context?.error).length
    ? (state as any).context?.error?.toast
    : (state as any).context?.success?.toast
  function toggleShow(): void {
    const { type } = (state && (state as any)).event
    type == 'DELAY_FAILURE' ? send('TO_FAILURE') : send('TO_SUCCESS')
  }

  function __renderIcon(notificationType) {
    switch (notificationType) {
      case 'toast-success':
        return <CheckIcon width={40} height={40} />
      case 'toast-error':
        return <CloseIcon width={40} height={40} />
      case 'toast-info':
        return <CheckIcon />
      case 'toast-warning':
        return <CheckIcon />

      default:
        return ''
    }
  }
  return (
    <Toast
      show={toast?.show ? toast?.show : false}
      onClose={toggleShow}
      delay={3000}
      autohide
      className={'uni-toast ' + toast?.type + ' top-right'}>
      <Toast.Header className="t-header"></Toast.Header>
      <Toast.Body className="body">
        <div className="d-flex justify-content-center align-items-center">
          {__renderIcon(toast?.type)}
        </div>
        <div className="mr-3">
          <p className="title">
            <strong className="mr-auto">{toast?.shortText}</strong>
          </p>
          <p className="subtitle">{toast?.longText}</p>
        </div>
      </Toast.Body>
    </Toast>
  )
}
export default ToastNotification
