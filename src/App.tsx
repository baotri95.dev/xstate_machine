// import React from 'react'
import { IphoneService } from '_chat_machine/chat.machine'
import { employeeModelConfig } from '_employee.machine/model/empolyee.config'
import { useEffect } from 'react'
import { Provider } from 'react-redux'
import { loginService } from './_login.machine/login.machine'
// import store from './store'
import AppRouter from './router/appRouter'
import { login_fr2Service } from '_system.machine/machine'
import { useSelector } from '@xstate/react'
import { useHistory } from 'react-router'
import { mergeMeta } from '_system.machine/options'

function App(): JSX.Element {
  return (
    <div>
      <AppRouter />
    </div>
  )
}

export default App
