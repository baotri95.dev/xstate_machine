import { createMachine, assign, spawn, send, sendParent } from 'xstate'
import { interpret } from 'xstate'
import { inspect } from '@xstate/inspect'
const server_machine = createMachine({
  initial: 'disconnected',
  states: {
    disconnected: {
      on: {
        CONNECT: 'connected',
      },
    },
    connected: {
      after: {
        2000: {
          actions: sendParent({ type: 'CONNECT.SUCCESS', isError: false }),
        },
      },
    },
  },
})

const client_machine = createMachine({
  id: 'client_machine',
  initial: 'waiting',
  context: {
    serverRef: null,
    isError: true,
  },
  states: {
    waiting: {
      entry: assign({
        serverRef: () => spawn(server_machine, 'server'),
      }),
      on: {
        'CONNECT.SERVER': {
          actions: send({ type: 'CONNECT' }, { to: 'server' }),
        },
        'CONNECT.SUCCESS': {
          cond: ctx => ctx.isError,
          target: 'connected',
        },
      },
    },
    connected: {},
  },
})

// OPENT FLOW CHART STATE
inspect({
  url: 'https://statecharts.io/inspect',
  iframe: false,
})

// Global Service

export const JoinService = interpret(client_machine, {
  devTools: true,
})
