import { useActor, useSelector } from '@xstate/react'
import ToastNotification from 'components/ToastNotification'
import { lazy, Suspense, useEffect } from 'react'
import { Breadcrumb } from 'react-bootstrap'
import {
  Link,
  Redirect,
  Route,
  RouteProps,
  Switch,
  useHistory,
} from 'react-router-dom'
import { login_fr2Service } from '_system.machine/machine'
import NAVLIST from './menu'
import PrivateLayout from './privateRouter'
const Home = lazy(() => import('../pages/login_fr2/home'))
const User = lazy(() => import('../pages/login_fr2/user'))

const Emoloyee = lazy(() => import('../pages/mvc-employee/employee.page'))
const IPHONE = lazy(() => import('../pages/chat/iphone.page'))
const SAMSUNGPAGE = lazy(() => import('../pages/chat/samsung.page'))
interface PrivateRouteProps extends RouteProps {
  isAuth: boolean
  machine: any
}

const LayoutMain = (props: PrivateRouteProps) => {
  const [state] = useActor(props?.machine)
  const { children } = state as any
  return (
    <div>
      <NAVLIST isAuth={props.isAuth} machine={props.machine}>
        {' '}
      </NAVLIST>
      <Switch>
        <PrivateLayout
          path="/admin/employee"
          isAuth={props.isAuth}
          component={Emoloyee}
          state={state}
          machineChildren={props?.machine?.children?.get('empMachine')}
        />
        <PrivateLayout
          path="/admin/chat_iphone"
          isAuth={props.isAuth}
          component={IPHONE}
        />
        <PrivateLayout
          path="/admin/chat_samsung"
          isAuth={props.isAuth}
          component={SAMSUNGPAGE}
        />
        <PrivateLayout
          path="/admin/home"
          isAuth={props.isAuth}
          component={Home}
          state={state}
        />
        <PrivateLayout
          path="/admin/user"
          isAuth={props.isAuth}
          component={User}
          state={state}
        />
        <Redirect to={'/admin/home'} />
      </Switch>
    </div>
  )
}
export default LayoutMain
