import { useActor, useSelector } from '@xstate/react'
import ToastNotification from 'components/ToastNotification'
import { useEffect } from 'react'
import { Redirect, Route, RouteProps, useHistory } from 'react-router-dom'
import { login_fr2Service } from '_system.machine/machine'
import { xPath } from '_system.machine/options'

interface PrivateRouteProps extends RouteProps {
  component: any
  isAuth: boolean
  machineChildren?: any
  state?: any
  allowRoles?: Array<string>
  allowServices?: Array<number>
}

const PrivateLayout = (props: PrivateRouteProps) => {
  const { isAuth, component: Component, state, ...rest } = props
  const history = useHistory()
  useEffect(() => {
    history.push(xPath(state?.meta))
  }, [state, props.machineChildren])

  return (
    <div>
      {props?.machineChildren ? (
        <ToastNotification machine={props.machineChildren} />
      ) : null}

      <Route
        exact
        {...rest}
        component={routeProps =>
          isAuth ? (
            <Component {...routeProps} machine={props?.machineChildren} />
          ) : (
            <Redirect to={`/auth/login`} />
          )
        }></Route>
    </div>
  )
}

export default PrivateLayout
