import { useActor } from '@xstate/react'
import { useEffect } from 'react'
import { Breadcrumb } from 'react-bootstrap'
import { Link, useHistory } from 'react-router-dom'
import { xPath } from '_system.machine/options'

const NAVLIST = (props: any) => {
  const history = useHistory()
  const [state, send] = useActor(props.machine) as any

  const goPage = (e, eventName) => {
    e.preventDefault()
    console.log(eventName)
    send(eventName)
  }
  useEffect(() => {
    history.push(xPath(state?.meta))
  }, [state])
  return (
    <div>
      {props.isAuth ? (
        <Breadcrumb>
          {' '}
          <Link to="/employee" onClick={e => goPage(e, 'GO_EMPLOYEE')}>
            Employee |
          </Link>
          <Link
            to="/admin/chat_iphone"
            onClick={e => goPage(e, 'GO_CHAT_IPHONE')}>
            Iphone |
          </Link>
          <Link
            to="/admin/chat_samsung"
            onClick={e => goPage(e, 'GO_CHAT_SAMSUNG')}>
            Samsung |
          </Link>
          <Link to={'/admin/home'} onClick={e => goPage(e, 'GO_HOME')}>
            Home |
          </Link>
          <Link to={'/admin/user'} onClick={e => goPage(e, 'GO_USER')}>
            User |
          </Link>
          <Link to={'/'} onClick={e => goPage(e, 'LOG_OUT')}>
            logout
          </Link>
        </Breadcrumb>
      ) : null}
    </div>
  )
}

export default NAVLIST
