import {
  BrowserRouter,
  Route,
  Link,
  Redirect,
  BrowserRouter as Router,
  Switch,
  useHistory,
  HashRouter,
} from 'react-router-dom'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
import { lazy, Suspense, useEffect, useMemo, useState } from 'react'
import { createBrowserHistory } from 'history'
import ToastNotification from 'components/ToastNotification'
import { useActor, useSelector } from '@xstate/react'
// import { XstateService } from 'machine/employee.machine'
import { login_fr2Service } from '_system.machine/machine'
import LayoutMain from './mainLayout'
import { xPath } from '_system.machine/options'
// import PrivateLayout from './privateRouter'

const Login = lazy(() => import('../pages/login_fr2/login'))

const AppRoute = () => {
  const [state] = useActor(login_fr2Service)
  const { dashboardRef } = state.context

  return (
    <Router>
      <Suspense fallback={false}>
        <Route path={`/auth/login`} component={Login}></Route>
        {state?.hasTag('isAuth') ? (
          /** State: Dashboard */
          <LayoutMain isAuth={state?.hasTag('isAuth')} machine={dashboardRef} />
        ) : (
          /** State: Ready */
          <Redirect to={`/auth/login`} />
        )}
      </Suspense>
    </Router>
  )
}

export default AppRoute
